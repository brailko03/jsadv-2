"use strict";



// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// Доречно використовувати try...catch під час отримання даних зовнішних джерел, в роботі з файлами, в роботі з великими обсягами данних. а



const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const container = document.querySelector("#root");


class MyError extends Error {
    constructor(message) {
        super(message)
        this.name = "MyError"
    }

}


books.forEach((book) => {

    try {

        if (!book.hasOwnProperty("author")) {
            throw new MyError("Error! There is no author")
        } else if (!book.hasOwnProperty("name")) {
            throw new MyError("Error! There is no name")
        } else if (!book.hasOwnProperty("price")) {
            throw new MyError("Error! There is no price")
        } else {
            container.insertAdjacentHTML("beforeend", `
            <ul>
               <li>${book.author}</li>
               <li>${book.name}</li>
               <li>${book.price}</li>
             </ul>
            `);
        }

    } catch (err) {
        console.log(err)
    }
});
